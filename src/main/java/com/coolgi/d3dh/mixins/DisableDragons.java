package com.coolgi.d3dh.mixins;

import com.seibel.distanthorizons.core.api.internal.ClientApi;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ClientApi.class)
public class DisableDragons {
    @Shadow
    private boolean configOverrideReminderPrinted;

    @Inject(at = @At("TAIL"), method = "Lcom/seibel/distanthorizons/core/api/internal/ClientApi;<init>()V")
    private void removeDragons(CallbackInfo ci) {
        configOverrideReminderPrinted = true;
    }
}
