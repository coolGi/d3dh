# Disable Distant Dragons
> Disables the dev message in [Distant Horizons](https://gitlab.com/jeseibel/distant-horizons)

Thats all...

## ⚠️WARNING⚠️: 
By using this project, you understand the dragons in Distant Horizons still exist, this project just hides the message\
Any damages that happen are not the fault of this project, or DH. We aren't liable for the dragons that appear, we only hide the warning about them


## What is this

The codebase is litrally a single mixin, and all the mod init stuff (`fabric.mod.json`, `quilt.mod.json`, `META-INF/mods.toml`)

Ummmmmmmmmm...

Well, this project supports Quilt, Fabric, Forge, NeoForge, and **every** Minecraft version in the same jar...\
soooo... download it if you know what you are doing and dont want the message in chat?
